<h1>My ToDo List</h1>
<ul>
    <#list todoitems as item>
        <li>
            ${item.id} - ${item.title} - <a href="/items/${item.id}/complete">Complete</a> - <a href="/items/${item.id}">View</a> - <a href="/items/${item.id}/delete">Delete</a>
        </li>
    </#list>
</ul>
<br />
<h2>Completed</h2>
<ul>
    <#list completeditems as item>
        <li>
            ${item.id} - ${item.title}
        </li>
    </#list>
</ul>
<br />
<br />
<a href="/items/new">New Item</a>