package ch.swissbytes.todo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

// selenium docs: http://docs.seleniumhq.org/docs/03_webdriver.jsp
public class ToDoListAcceptanceTest {
    private static WebDriver driver;
    private static final ToDoList app = new ToDoList();

    @BeforeClass
    public static void setup() throws InterruptedException {
       app.startup(new EntityManager());
       Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines

        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown(){
        app.shutdown();
    }

    @Test
    public void willListTodoItems() {
        driver.get("http://localhost:4567/items");
        WebElement newItemLink = driver.findElement(By.linkText("New Item"));
        newItemLink.click();
    }
}
